package com.e2e.pages.authentication;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage {
    private WebDriver driver;
    private By successMessage = By.xpath("//*[contains(text(),'Georgi Kazandzhiev')]");

    public DashboardPage(WebDriver driver) {
                this.driver = driver;
    }

    public String getSuccessMessage() {
                return driver.findElement(successMessage).getText();
    }
}