package com.e2e.pages.authentication;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
    private WebDriver driver;
    private By emailInput = By.name("email");
    private By nameInput = By.name("name");
    private By passwordInput = By.name("password");
    private By loginButton = By.xpath("//button[contains(text(),'Login')]");
    @FindBy(xpath = "//input[@data-qa='signup-name']")
    private WebElement loc_inputName;
    @FindBy(xpath = "//input[@data-qa='signup-email']")
    private WebElement loc_inputEmail;
    @FindBy(xpath = "//button[@data-qa='signup-button']")
    private WebElement loc_btnSignup;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void enterEmail(String email) {

        driver.findElement(emailInput).sendKeys(email);
    }

    public void enterPassword(String password) {

        driver.findElement(passwordInput).sendKeys(password);
    }

    public void clickLoginButton() {

        driver.findElement(loginButton).click();
    }
    public void enterName(String name){
        driver.findElement(nameInput).sendKeys(name);
    }


}