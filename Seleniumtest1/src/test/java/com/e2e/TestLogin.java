package com.e2e;

import com.e2e.pages.authentication.DashboardPage;
import com.e2e.pages.authentication.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestLogin {
    private WebDriver driver;
    private String baseUrl = "https://automationexercise.com/login";

    @BeforeMethod
    public void setUp() {
//        System.setProperty("webdriver.chrome.driver", "path/to/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testSuccessfulLogin() throws InterruptedException {
        driver.get(baseUrl);

        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterEmail("test.test@example.com");
        loginPage.enterPassword("password");
        loginPage.clickLoginButton();

        DashboardPage dashboardPage = new DashboardPage(driver);
        String successMessage = dashboardPage.getSuccessMessage();
        Assert.assertEquals(successMessage, "Georgi Kazandzhiev");

        Thread.sleep(5000);
    }
}
